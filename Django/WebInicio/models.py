from django.db import models

# Create your models here.
class Personas(models.Model):
    ID = models.IntegerField(primary_key=True)
    NOMBRE = models.TextField(max_length=20)
    APELLIDO = models.TextField(max_length=20)
    USUARIO = models.TextField(max_length=20)
    CLAVE = models.TextField(max_length=20)