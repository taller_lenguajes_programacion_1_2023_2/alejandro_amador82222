from django.urls import path
from .views import paginaInicio,paginaRegister

urlpatterns = [
    path('',paginaInicio),
    path('register/',paginaRegister)
]