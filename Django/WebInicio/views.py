from django.shortcuts import render, redirect
from .models import Personas

def paginaInicio(request):
    return render(request,'inicio.html')

def paginaRegister(request):
    if request.method == 'POST':
        nombre = request.POST['nombre']
        apellido = request.POST['apellido']
        usuario = request.POST['usuario']
        contrasena = request.POST['contrasena']
        nuevoDato = Personas.objects.create(NOMBRE=nombre, APELLIDO=apellido, USUARIO=usuario, CLAVE=contrasena)
        return redirect('/')
        
    return render(request, 'register.html')

def paginaLogin(request):
    error = ""
    if request.method == 'POST':
        try:
            usuario = request.POST['usuario']
            clave = request.POST['clave']
            verificarUsuario = Personas.objects.get(USUARIO=usuario)
            if verificarUsuario.CLAVE == clave:
                return redirect('/')
            else:
                error = "La contraseña ingresada es incorrecta."
        except:
            error = "El usuario no esta registrado."
            
    return render(request, 'Login.html', {
        "error":error
    })

# Para cambiar datos en la base de datos
#   try:
#       usuario = request.POST['usuario']
#       verificarUsuario = Personas.objects.get(USUARIO=usuario)
#       verificarUsuario.COLUMNA = NuevoValor
#       verificarUsuario.save()
#   except:
#       El usuario no esta registrado

# Para eliminar datos en la base de datos
#   try:
#       usuario = request.POST['usuario']
#       verificarUsuario = Personas.objects.get(USUARIO=usuario)
#       verificarUsuario.delete()
#   except:
#       El usuario no esta registrado