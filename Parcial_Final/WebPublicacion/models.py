from django.db import models

class Publicacion(models.Model):
    ID = models.IntegerField(primary_key=True)
    TITULO = models.TextField(max_length=30)
    JUEGO = models.TextField(max_length=30)
    AUTOR = models.TextField(max_length=30)
    DESCRIPCION = models.TextField()
    IMAGEN = models.TextField()
    LIKES = models.IntegerField()