from django.shortcuts import render,redirect
from WebPublicacion.models import Publicacion
from WebUsuario.models import Usuario

def paginaWall(request):
    if request.method == "POST":
        juego = request.POST["juego"]
        if juego == "Todos":
            return render(request, 'wall.html', {
            "usuario" : request.session.get("usuario",None),
            "correo" : request.session.get("correo",None),
            "publicaciones" : reversed(Publicacion.objects.all())
        }) 
        else:
            return render(request, 'wall.html', {
                "usuario" : request.session.get("usuario",None),
                "correo" : request.session.get("correo",None),
                "publicaciones" : reversed(Publicacion.objects.filter(JUEGO = juego))
        })
    return render(request, 'wall.html', {
        "usuario" : request.session.get("usuario",None),
        "correo" : request.session.get("correo",None),
        "publicaciones" : reversed(Publicacion.objects.all())
    })
    
def paginaUpload(request):
    mensaje=""
    if request.method == "POST":
        titulo = request.POST['titulo']
        juego = request.POST['juego']
        descripcion = request.POST['descripcion']
        imagen = request.POST['imagen']
        autor = request.session.get("usuario",None)
        try: 
            publicacion = Publicacion.objects.get(TITULO = titulo,AUTOR = autor, JUEGO = juego)
            mensaje = "Ya tienes una reseña con este título en este juego"
        except:
            try:
                publicacion = Publicacion.objects.create(TITULO = titulo, JUEGO = juego, DESCRIPCION = descripcion, IMAGEN = imagen, LIKES = 0, AUTOR = autor)
                mensaje = "La reseña se ha publicado con exito."
            except:
                mensaje = "Hubo un error al publicar la reseña."
        
    return render(request, 'upload.html',{
        "mensaje" : mensaje,
    })
    
def paginaAccount(request):
    return render(request, 'account.html', {
        "usuario" : request.session.get("usuario",None),
        "correo" : request.session.get("correo",None),
        "publicaciones" : reversed(Publicacion.objects.filter(AUTOR = request.session.get("usuario",None)))
    })

def paginaPublicacion(request,publicacion):
    if request.method == "POST":
        publicacion = Publicacion.objects.get(ID = publicacion)
        publicacion.LIKES += 1
        publicacion.save()
        return render (request, 'publicacion.html', {
            "publicacion" : publicacion,
        })
        
    return render (request, 'publicacion.html', {
        "publicacion" : Publicacion.objects.get(ID = publicacion),
    })
    
def paginaAnotherAccount (request,usuario):
    usuario = Usuario.objects.get(USUARIO=usuario)
    return render(request, 'another_account.html', {
        "usuario" : usuario.USUARIO,
        "correo" : usuario.CORREO,
        "publicaciones" : reversed(Publicacion.objects.filter(AUTOR = usuario.USUARIO))
    })
    
def paginaAdminPublicacion (request,publicacion):
    if request.method == "POST":
        publicacion = Publicacion.objects.get(ID = publicacion)
        publicacion.delete()
        return redirect('/account')
        
    return render (request, 'admin_publicacion.html', {
        "publicacion" : Publicacion.objects.get(ID = publicacion),
    })

# Publicacion.objects.get(ID = 3).delete()