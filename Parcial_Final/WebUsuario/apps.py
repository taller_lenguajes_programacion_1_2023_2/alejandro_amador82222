from django.apps import AppConfig


class WebusuarioConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'WebUsuario'
