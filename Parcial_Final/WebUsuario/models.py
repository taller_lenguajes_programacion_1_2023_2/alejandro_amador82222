from django.db import models

class Usuario(models.Model):
    ID = models.IntegerField(primary_key=True)
    USUARIO = models.TextField(max_length=30)
    CORREO = models.TextField(max_length=50)
    CONTRASENA = models.TextField(max_length=50)