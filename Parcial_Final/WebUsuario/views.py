from django.shortcuts import render, redirect
from .models import Usuario

def paginaIndex(request):
    return render (request,'index.html')

def paginaRegister(request):
    error = ""
    if request.method == "POST":
        usuario = request.POST['usuario']
        correo = request.POST['correo']
        contrasena1 = request.POST['contrasena1']
        contrasena2 = request.POST['contrasena2']
        try:
            Usuario.objects.get(USUARIO=usuario)
            error = "El Usuario ya está registrado."
        except:
            if contrasena1==contrasena2:
                nuevoUser = Usuario.objects.create(USUARIO=usuario,CORREO=correo,CONTRASENA=contrasena1)
                error="Se ha registrado con éxito."
            else: 
                error="Las contraseñas no coinciden."
    return render(request,'register.html',{
        "error":error
    })
    
def paginaLogin(request): 
    mensaje = ""
    if request.method=="POST":
        usuario = request.POST['usuario']
        contrasena = request.POST['contrasena']
        
        try:
            user = Usuario.objects.get(USUARIO = usuario)
            if contrasena == user.CONTRASENA:
                request.session["usuario"]=user.USUARIO
                request.session["correo"]=user.CORREO
                return redirect("/wall")
            else:
                mensaje = "La contraseña ingresada es incorrecta."
        except:
            mensaje = "El usuario ingresado no se encuentra registrado."
        
    return render(request,'login.html',{
        "mensaje" : mensaje
    })
    
# Usuario.objects.get(ID = 12).delete()