function cerrarSesion() {
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: "btn btn-success",
          cancelButton: "btn btn-danger"
        },
        buttonsStyling: false
      });
      swalWithBootstrapButtons.fire({
        title: "¿Cerrar Sesión?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, cerrar sesión",
        cancelButtonText: "No, cancelar",
        reverseButtons: false
      }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = '/';
        }
      });
}