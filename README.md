## Alejandro Amador Ruiz

Bienvenido a mi repositorio.

## Fechas de Evaluación

1. **Entregable 1**: 7 de Septiembre, 2023
2. **Entregable 2**: 28 de Septiembre, 2023
3. **Parcial 1**: 5 Octubre, 2023
4. **Entregable 3**: 2 Noviembre, 2023
5. **Entregable 4**: 30 Noviembre, 2023
6. **Parcial Final**: 7 Diciembre, 2023

## Sesiones

| Sesión | Fecha      | Tema                                                                                                                       |
| ------- | ---------- | ------------------------------------------------------------------------------------------------------------------------- |
| 1       | 10/08/2023 | Introducción a la programación                                                                                            |
| 2       | 16/08/2023 | Variables y Tipos de Variables, estructura de datos, lectura y escritura de archivos ".src\tp1_sesiones\tp1_s2.py" |
| 3       | 17/08/2023 | lectura y escritura de archivos planos y excel ".src\tp1_sesiones\tp1_s3.py"                                               |
| 4       | 23/08/2023 | lectura y escritura de archivos planos y excel repaso ".src\tp1_sesiones\tp1_s4.py"                                               |
| 5       | 24/08/2023 | lectura y escritura de archivos planos y excel ".src\tp1_sesiones\tp1_s5.py"                                               |
| 6       | 30/08/2023 | Parte 1 del taller 1 de practica ".src\tp1_sesiones\s6 taller 1.py"                                               |
| 7       | 31/08/2023 | Parte 2 del taller 1 de practica ".src\tp1_sesiones\s7 taller 1 pt2.py"            
| 8       | 06/08/2023 | Programación orientada a objetos                                       |
| 9       | 13/08/2023 | Programación orientada a objetos con herencia                                       |
| 10       | 20/08/2023 | conectar base de datos de sqlite con POO                                     |