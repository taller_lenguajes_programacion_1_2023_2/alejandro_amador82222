import tkinter as tk
from tkinter import ttk, messagebox
import sv_ttk
from pelicula import Pelicula

class Frame(tk.Frame):
    def __init__(self,root=None):
        super().__init__(root,width=480,height=320)
        self.root = root
        sv_ttk.use_dark_theme()
        self.pack()
        self.pelicula = object
        #self.config(bg="green") cambia el color de la pantalla
        self.camposPelicula()
        
    def camposPelicula(self):
        self.lbl_nombre = tk.Label(self, text = "Nombre: ")
        self.lbl_nombre.config(font=("Calibri",12,"bold"))
        self.lbl_nombre.grid(row=0,column=0,padx=9,pady=9)
        
        self.lbl_duracion = tk.Label(self, text = "Duracion: ")
        self.lbl_duracion.config(font=("Calibri",12,"bold"))
        self.lbl_duracion.grid(row=1,column=0,padx=9,pady=9)
        
        self.lbl_genero = tk.Label(self, text = "Genero: ")
        self.lbl_genero.config(font=("Calibri",12,"bold"))
        self.lbl_genero.grid(row=2,column=0,padx=9,pady=9)
        
        self.var_nombre = tk.StringVar()
        self.input_nombre = tk.Entry(self,textvariable=self.var_nombre)
        self.input_nombre.config(width=50,font=("Calibri",12))
        self.input_nombre.grid(row=0,column=1,padx=10,pady=10)
        
        self.var_duracion = tk.StringVar()
        self.input_duracion = tk.Entry(self,textvariable=self.var_duracion)
        self.input_duracion.config(width=50,font=("Calibri",12))
        self.input_duracion.grid(row=1,column=1,padx=10,pady=10)
        
        self.var_genero = tk.StringVar()
        self.input_genero = tk.Entry(self,textvariable=self.var_genero)
        self.input_genero.config(width=50,font=("Calibri",12))
        self.input_genero.grid(row=2,column=1,padx=10,pady=10)
        
        #botones
        self.btn_nuevo = tk.Button(self,text="Nuevo",command=self.habilitarCampos)
        self.btn_nuevo.config(width=20, font=("Calibri",12))
        self.btn_nuevo.grid(row=3,column=0,padx=10,pady=10)
        
        self.btn_guardar = tk.Button(self,text="Guardar",command=self.guardarDatos)
        self.btn_guardar.config(width=20, font=("Calibri",12))
        self.btn_guardar.grid(row=3,column=1,padx=10,pady=10)
        
        self.btn_cancelar = tk.Button(self,text="Cancelar",command=self.deshabilitarCampos)
        self.btn_cancelar.config(width=20, font=("Calibri",12))
        self.btn_cancelar.grid(row=3,column=2,padx=10,pady=10)
    
    def habilitarCampos(self):
        self.var_nombre.set("")
        self.var_duracion.set("")
        self.var_genero.set("")
        
        self.input_nombre.config(state="normal")
        self.input_duracion.config(state="normal")
        self.input_genero.config(state="normal")
        
        self.btn_cancelar.config(state="normal")
        self.btn_guardar.config(state="normal")
    
    def deshabilitarCampos(self):
        self.input_nombre.config(state="disabled")
        self.input_duracion.config(state="disabled")
        self.input_genero.config(state="disabled")
        
        self.btn_cancelar.config(state="disabled")
        self.btn_guardar.config(state="disabled")
    
    def guardarDatos(self):
        self.pelicula = Pelicula(self.var_nombre.get(),self.var_duracion.get(),self.var_genero.get())
        if self.pelicula.id_pelicula == None:
            self.pelicula.guardar()
        else:
            self.pelicula.editar(self.pelicula.id_pelicula)
    
    def tablaPeliculas(self):
        self.lista_peliculas = self.pelicula.listar()
        
        self.tabla = ttk.Treeview(self,columns=('Nombre','Duracion','Genero'))
        self.tabla.grid(row=4,column=0, columnspan=4,sticky='')
        
        self.scroll = ttk.Scrollbar(self,orient="vertical",command=self.tabla.yview)
        self.tabla.configure(yscrollcommand=self.scroll.set)
        
        
    
    def editarDatos(self):
        pass
    
    def eliminarPeliculas(self):
        pass