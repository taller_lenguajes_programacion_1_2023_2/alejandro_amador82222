import sqlite3
from tkinter import messagebox

class Conexion:
    def __init__(self):
        self.ruta_db = "./static/db/peliculas.db"
        self.conexion = sqlite3.connect(self.ruta_db)
        self.cursor = self.conexion.cursor()
        
    def crear_tabla(self):
        query="""
        CREATE TABLE peliculas (id_pelicula INTEGER, nombre VARCHAR(100), duracion VARCHAR(10), genero VARCHAR(100), PRIMARY KEY (id_pelicula AUTOINCREMENT))
        """
        try: 
            self.cursor.execute(query)
            self.cerrar()
            messagebox.showinfo("Info","Se ha creado la tabla.")
        except:
            messagebox.showerror("Error","No se pudo crear la tabla.")
            
    def eliminar_tabla(self):
        query="""
        CREATE TABLE peliculas (id_pelicula INTEGER, nombre VARCHAR(100), duracion VARCHAR(10), genero VARCHAR(100), PRIMARY KEY (id_pelicula AUTOINCREMENT))
        """
        try: 
            self.cursor.execute(query)
            self.cerrar()
            messagebox.showinfo("Info","Se ha eliminado la tabla.")
        except:
            messagebox.showerror("Error","No se pudo eliminar la tabla.")
            
    def cerrar(self):
        self.conexion.commit()
        self.conexion.close()
        
cnx = Conexion()