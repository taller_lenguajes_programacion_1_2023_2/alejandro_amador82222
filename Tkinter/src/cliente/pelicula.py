#from conexion import Conexion
from tkinter import messagebox
from conexion import Conexion
import json

class Pelicula(Conexion):
    def __init__(self,nombre,duracion,genero):
        self.id_pelicula = None
        self.nombre = nombre
        self.duracion = duracion
        self.genero = genero
        self.conexion = Conexion()
        self.cursor = self.conexion.cursor
        
    def guardar(self):
        query = """
        INSERT INTO peliculas (nombre,duracion,genero)
        VALUES ('{}','{}',{})
        """.format(self.nombre,self.duracion,self.genero)
        try:
            self.cursor.execute(query)
            self.conexion.cerrar()
        except Exception as error:
            print(error)
            
    def editar(self,id_pelicula="",persona=object):
        if id_pelicula!="":
            query = """
            UPDATE peliculas
            SET nombre = '{}', duracion = '{}', genero = '{}'
            WHERE id_pelicula={}
            """.format(self.nombre,self.duracion,self.genero,id_pelicula)
            try:
                self.cursor.execute(query)
                self.conexion.cerrar()
            except Exception as error:
                print(error)
        else:
            print("No se dió una id.")
            
    def eliminar(self,id_pelicula=""):
        if id_pelicula!="":
            query = """
            DELETE FROM peliculas WHERE id_pelicula = {}
            """.format(id_pelicula)
            try:
                self.cursor.execute(query)
                self.conexion.cerrar()
                messagebox.showinfo("Exito","La eliminación se ha realizado con exito.")
            except Exception as error:
                print(error)
                messagebox.showerror("Error","No se pudo realizar la eliminacion.")
        else:
            print("Error: No ha sido dada una id.")
            
    def listar(self):
        query = """
        SELECT * FROM peliculas 
        """
        try:
            self.cursor.execute(query)
            self.conexion.cerrar()
        except Exception as error:
            print(error)
        
    def __str__(self):
        return f"Pelicula [{self.nombre}, {self.duracion}, {self.genero}]"
    
pelicula = Pelicula("hola","como","estas")