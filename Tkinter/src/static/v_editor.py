import tkinter as tk
from tkinter.filedialog import askopenfile, asksaveasfilename
import sv_ttk

class Editor(tk.Tk):
    def __init__ (self):
        super().__init__()
        self.title("Editor de Texto")
        self.rowconfigure(0,minsize=600,weight=1)
        self.columnconfigure(1,minsize = 600, weight=1)
        #atributo de campo de texto
        self.campoDeTexto = tk.Text(self,wrap=tk.WORD)
        self.archivo = None
        self.archivoAbierto = None
        self._crearComponente()
        self.geometry("600x600")
    
    def _crearComponente(self):
        frame_botones = tk.Frame(self,relief=tk.RAISED,bd=2)
        boton_abrir=tk.Button(frame_botones,text="Abrir",command=self._abrirArchivo)
        boton_guardar=tk.Button(frame_botones,text="Guardar",command=self._guardarArchivo)
        
        boton_abrir.grid(row=0,column=0,sticky="we",padx=5,pady=5)
        boton_guardar.grid(row=1,column=0,sticky="we",padx=5,pady=5)
        frame_botones.grid(row=0,column=0,sticky="ns")
        self.campoDeTexto.grid(row=0,column=1,sticky="nswe")
        
    def _abrirArchivo(self):
        self.archivoAbierto = askopenfile(mode="r+")
        self.campoDeTexto.delete(1.0,tk.END)
        if not self.archivoAbierto:
            return
        with open(self.archivoAbierto.name,"r+") as self.archivo:
            texto = self.archivo.read()
            self.campoDeTexto.insert(1.0,texto)
            self.title(f"Editor de Texto - {self.archivo.name}")
            
    def _guardarArchivo(self):
        if self.archivoAbierto:
            with open (self.archivoAbierto.name,"w") as self.archivo:
                self.archivoAbierto.write(self.campoDeTexto.get(1.0,tk.END))
        else:
            self._guardarComo()
    
    def _guardarComo(self):
        pass            
              
if __name__ == "__main__":
    editor = Editor()
    sv_ttk.set_theme("dark")
    editor.mainloop()