import tkinter as tk #VENTANAS
from tkinter import ttk #COMPONENTES

class Ventana:
    def __init__(self,dimension="800x600",titulo="Ventana"):
        self.dimension = dimension
        self.titulo = titulo
        self.vtna = self.crearVentana()
    
    def crearVentana(self):
        ventana = tk.Tk()
        ventana.geometry(self.dimension)
        ventana.title(self.titulo)
        ventana.iconbitmap("Tkinter/src/static/img/icon.ico")
        boton1 = ttk.Button(ventana,text="EJECUTAR")
        boton1.bind("<Button-1>", lambda event: self.eventoBoton1(boton1))
        entrada1 = ttk.Entry(ventana,width=30,justify=tk.CENTER)
        entrada1.config(background="red")
        entrada1.bind("Alejo",lambda event: self.eventoEntrada1(entrada1))
        
        entrada1.pack(expand=True,anchor="center")
        boton1.pack(expand=True,anchor="center")
        return ventana
    
    def eventoEntrada1(self,entrada):
        print("ALEJO")
    
    def eventoBoton1(self,boton):
        boton.config(text="EJECUTADO",state="disabled")
        print("Boton presionado")
        
    def eventoBoton2(self,boton):
        pass
    
    def mostrarVentana(self):
        self.vtna.mainloop()
        
vtna = Ventana("800x800","ALEJO")
vtna.mostrarVentana()