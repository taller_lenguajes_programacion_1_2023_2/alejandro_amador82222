from conexion import Conexion
from tkinter import messagebox

class Alien(Conexion):
    def __init__(self,nombreAlien="",idPlaneta=0):
        """Crea un objeto Alien

        Args:
            nombreAlien (str): Nombre del alien
            idPlaneta (int): Id del planeta donde vive el alien
        """
        super().__init__()
        self.id = None
        self.nombreAlien = nombreAlien
        self.idPlaneta = idPlaneta
        
    # GUARDAR ALIEN EN LA BASE DE DATOS
    def guardarAlien(self,IdsPlanetas):
        """Guarda el alien en la base de datos

        Args:
            IdsPlanetas (list): lista de ids de los planetas que están registrados en la base de datos
        """
        auxId=False
        for i in IdsPlanetas:
            if self.idPlaneta == i[0]:
                auxId=True # MIRA QUE EL ALIEN VIVA EN UN PLANETA QUE ESTÉ REGISTRADO EN LA BASE DE DATOS
        if self.nombreAlien!="" and self.idPlaneta!="":
            if auxId:
                self.cursor.executemany(self.queries["InsertarDatos"].format("Aliens","?,?"),[(self.nombreAlien,self.idPlaneta)])
                self.conexion.commit()
                messagebox.showinfo("Info","El Alien se ha guardado con éxito.")
            else:
                messagebox.showerror("Error","La Id del planeta no es una Id de planeta existente en la tabla planetas.")
        else:
            messagebox.showerror("Error","El campo Nombre o el campo Id Planeta están vacíos.")

    # VER ALIENS 
    def verAliens(self):
        """Obtener los aliens de la base de datos

        Returns:
            list: Lista que contiene los aliens de la db
        """
        if self.nombreAlien!="" and self.idPlaneta!="":
            self.cursor.execute(self.queries["LeerDatos"].format("Aliens"))
            return self.cursor.fetchall()
        else:
            print("Error en ver Aliens")
            
    # ACTUALIZAR ALIEN
    def editarAlien(self,id,IdsPlanetas):
        """Actualiza la información del alien en cuestion

        Args:
            id (int): id del alien que será actualizado
            IdsPlanetas (list): lista de ids de los planetas que están registrados en la base de datos
        """
        auxId = False
        self.cursor.execute(self.queries["LeerId"].format("Aliens"))
        for i in self.cursor.fetchall():
            if id == i[0]:
                auxId=True  # MIRA QUE LA ID DEL ALIEN EXISTA
        if auxId:
            auxId=False
            for i in IdsPlanetas:
                if self.idPlaneta == i[0]:
                    auxId=True # MIRA QUE EL ALIEN VIVA EN UN PLANETA QUE ESTÉ REGISTRADO EN LA BASE DE DATO
            if self.nombreAlien!="" and self.idPlaneta!="":
                if auxId:
                    self.cursor.execute(self.queries["ActualizarDatos"].format("Aliens","Alien",self.nombreAlien,"Id",id))
                    self.cursor.execute(self.queries["ActualizarDatos"].format("Aliens","IdPlaneta",self.idPlaneta,"Id",id))
                    self.conexion.commit()
                    messagebox.showinfo("Info","El Alien se ha actualizado con éxito.")
                else:
                    messagebox.showerror("Error","La Id del planeta no es una Id de planeta existente en la tabla planetas.")
            else:
                messagebox.showerror("Error","El campo Nombre o el campo id Planeta están vacíos.")
        else:
            messagebox.showerror("Error","El ID seleccionado no existe.")
       
    # ELIMINAR ALIEN     
    def eliminarAlien(self,id=0):
        """Elimina el alien con el que coincida el id en la base de datos

        Args:
            id (int): id del alien que será eliminado
        """
        auxId = False
        self.cursor.execute(self.queries["LeerId"].format("Aliens"))
        for i in self.cursor.fetchall():
            if id == i[0]:
                auxId=True # MIRA QUE LA ID DEL ALIEN EXISTA
        if auxId:
            self.cursor.execute(self.queries["EliminarDatos"].format("Aliens","Id",id))
            self.conexion.commit()
            messagebox.showinfo("Info","El Alien se ha eliminado con éxito.")
        else:
            messagebox.showerror("Error","El ID seleccionado no existe.")
      
    # ELIMINAR ALIENS QUE VIVEN EN UN PLANETA ELIMINADO      
    def eliminarAlienDePlaneta(self,id=0):
        """Elimina los aliens que pertenezcan a la id del planeta del argumento
        
        Args:
            id (int): Id del planeta    
        """
        if id!="":
            self.cursor.execute(self.queries["EliminarDatos"].format("Aliens","IdPlaneta",id))
            self.conexion.commit()
            messagebox.showinfo("Info","El o los aliens de este planeta se han eliminado con exito.")
        else:
            messagebox.showerror("Error","El ID seleccionado no existe.")