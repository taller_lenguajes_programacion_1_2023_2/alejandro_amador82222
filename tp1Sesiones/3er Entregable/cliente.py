import tkinter as tk
from tkinter import messagebox,ttk
from planetas import Planeta
from alien import Alien
import sv_ttk

class Cliente():
    def __init__(self):
        """Crea un objeto Cliente
        """
        self.ventana = tk.Tk()
        self.planetaMain = Planeta("main","main") # Con este objeto Planeta se llaman los metodos de mostrar y eliminar
        self.alienMain = Alien("main","main") # Con este objeto Alien se llaman los metodos de mostrar y eliminar
        self.ventana.title("CLIENTE PLANETAS Y ALIENS")
        self.ventana.resizable(False,False)
        self.ventana.iconbitmap("./tp1Sesiones/3er Entregable/icono.ico") # Icono representativo del programa
        self.cargarWidgets() # Carga casi toda la interfaz
        self.ventana.config(menu=self.menu) # Carga el menú que contiene el botón para cambiar tema
        self.mostrarPlanetas() #estas dos funciones son las que cargan y llenan las tablas de la base de datos en la interfaz
        self.mostrarAliens()
        sv_ttk.use_dark_theme()
        self.ventana.mainloop()
        
    def cargarWidgets(self):
        """Carga los widgets de la interfaz
        """
        # MENU (Cambio de tema)
        self.menu = tk.Menu(tearoff=0,title="Cambiar tema")
        self.menu.add_command(command=self.cambiarTema, label="Cambiar Tema")
        
        # LABELS PLANETAS
        self.lbl_nombrePlaneta = tk.Label(text="PLANETAS",font=("Cascadia Code",40,"bold"),foreground="#AC65BF") #Titulo
        self.lbl_nombrePlaneta.grid(row=0,column=0,padx=10,pady=10,columnspan=2)
        self.lbl_nombrePlaneta = tk.Label(text="NOMBRE:",font=("Cascadia Code",20,"")) #Nombre
        self.lbl_nombrePlaneta.grid(row=1,column=0,padx=10,pady=10)
        self.lbl_galaxia = tk.Label(text="GALAXIA:",font=("Cascadia Code",20,"")) #Galaxia
        self.lbl_galaxia.grid(row=2,column=0,padx=10,pady=10)
        self.lbl_idPlanetaP = tk.Label(text="EDITAR CON \nID:",font=("Cascadia Code",20,"")) #IDPlaneta
        self.lbl_idPlanetaP.grid(row=0,column=3,padx=10,pady=10)
        
        # LABELS ALIENS
        self.lbl_nombreAliens = tk.Label(text="ALIENS",font=("Cascadia Code",40,"bold"),foreground="#AC65BF") #Titulo
        self.lbl_nombreAliens.grid(row=0,column=5,padx=10,pady=10,columnspan=2)
        self.lbl_nombreAlien = tk.Label(text="NOMBRE:",font=("Cascadia Code",20,"")) #Nombre
        self.lbl_nombreAlien.grid(row=1,column=5,padx=10,pady=10)
        self.lbl_idPlaneta = tk.Label(text="ID PLANETA:",font=("Cascadia Code",20,"")) #IDPlaneta
        self.lbl_idPlaneta.grid(row=2,column=5,padx=10,pady=10)
        self.lbl_idAlien = tk.Label(text="EDITAR CON \nID:",font=("Cascadia Code",20,"")) #IDAlien
        self.lbl_idAlien.grid(row=0,column=8,padx=10,pady=10)
        
        # CAMPOS DE TEXTO PLANETAS
        self.varNombrePlaneta = tk.StringVar()
        self.txt_nombrePlaneta = tk.Entry(textvariable=self.varNombrePlaneta,font=("Cascadia Code",12,""),borderwidth=3) #Nombre
        self.txt_nombrePlaneta.grid(row=1,column=1,padx=10,pady=10)
        self.varGalaxia = tk.StringVar()
        self.txt_galaxia = tk.Entry(textvariable=self.varGalaxia,font=("Cascadia Code",12,""),borderwidth=3) #Galaxia
        self.txt_galaxia.grid(row=2,column=1,padx=10,pady=10)
        self.varIdPlaneta = tk.IntVar()
        self.txt_idPlaneta = tk.Entry(textvariable=self.varIdPlaneta,font=("Cascadia Code",12,""),borderwidth=3) #ID
        self.txt_idPlaneta.grid(row=1,column=3,padx=10,pady=10)
        
        # CAMPOS DE TEXTO ALIENS
        self.varNombreAlien = tk.StringVar()
        self.txt_nombreAlien = tk.Entry(textvariable=self.varNombreAlien,font=("Cascadia Code",12,""),borderwidth=3) #Nombre
        self.txt_nombreAlien.grid(row=1,column=6,padx=10,pady=10)
        self.varIdPlanetaAlien = tk.IntVar()
        self.txt_idPlanetaAlien = tk.Entry(textvariable=self.varIdPlanetaAlien,font=("Cascadia Code",12,""),borderwidth=3) #IdPlaneta
        self.txt_idPlanetaAlien.grid(row=2,column=6,padx=10,pady=10)
        self.varIdAlien = tk.IntVar()
        self.txt_idAlien = tk.Entry(textvariable=self.varIdAlien,font=("Cascadia Code",12,""),borderwidth=3) #ID
        self.txt_idAlien.grid(row=1,column=8,padx=10,pady=10)
        
        # TABLA PLANETAS
        self.tabla_planetas = ttk.Treeview(columns=("ID","Planeta","Galaxia"),show="headings")
        self.tabla_planetas.heading("ID",text="ID")
        self.tabla_planetas.heading("Planeta",text="PLANETA")
        self.tabla_planetas.heading("Galaxia",text="GALAXIA")
        self.tabla_planetas.grid(row=4,column=0,columnspan=4,padx=10,pady=10)
        
        # TABLA ALIENS
        self.tabla_aliens = ttk.Treeview(columns=("ID","Alien","IdPlaneta"),show="headings",)
        self.tabla_aliens.heading("ID",text="ID")
        self.tabla_aliens.heading("Alien",text="ALIEN")
        self.tabla_aliens.heading("IdPlaneta",text="ID PLANETA")
        self.tabla_aliens.grid(row=4,column=5,columnspan=4,padx=10,pady=10)
        
        # BOTONES PLANETAS
        self.btn_nuevoPlaneta = tk.Button(text="GUARDAR",command=self.guardarPlaneta,background="#AC65BF",font=("Cascadia Code",12,""),borderwidth=3) #Guardar
        self.btn_nuevoPlaneta.grid(row=3,column=0,padx=10,pady=10)
        self.btn_limpiarPlaneta = tk.Button(text="LIMPIAR",command=self.limpiarPlaneta,background="#AC65BF",font=("Cascadia Code",12,""),borderwidth=3) #Limpiar
        self.btn_limpiarPlaneta.grid(row=3,column=1,padx=10,pady=10)
        self.btn_editarPlaneta = tk.Button(text="ACTUALIZAR",command=self.editarPlaneta,background="#AC65BF",font=("Cascadia Code",12,""),borderwidth=3) #Actualizar
        self.btn_editarPlaneta.grid(row=2,column=3,padx=10,pady=10)
        self.btn_eliminarPlaneta = tk.Button(text="ELIMINAR",command=self.eliminarPlaneta,background="#AC65BF",font=("Cascadia Code",12,""),borderwidth=3) #Eliminar
        self.btn_eliminarPlaneta.grid(row=3,column=3,padx=10,pady=10)
        
        # BOTONES ALIENS
        self.btn_nuevoAlien = tk.Button(text="GUARDAR",command=self.guardarAlien,background="#AC65BF",font=("Cascadia Code",12,""),borderwidth=3) #Guardar
        self.btn_nuevoAlien.grid(row=3,column=5,padx=10,pady=10)
        self.btn_limpiarAlien = tk.Button(text="LIMPIAR",command=self.limpiarAlien,background="#AC65BF",font=("Cascadia Code",12,""),borderwidth=3) #Limpiar
        self.btn_limpiarAlien.grid(row=3,column=6,padx=10,pady=10)
        self.btn_editarAlien = tk.Button(text="ACTUALIZAR",command=self.editarAlien,background="#AC65BF",font=("Cascadia Code",12,""),borderwidth=3) #Actualizar
        self.btn_editarAlien.grid(row=2,column=8,padx=10,pady=10)
        self.btn_eliminarAlien = tk.Button(text="ELIMINAR",command=self.eliminarAlien,background="#AC65BF",font=("Cascadia Code",12,""),borderwidth=3) #Eliminar
        self.btn_eliminarAlien.grid(row=3,column=8,padx=10,pady=10)
        
        # SEPARADORES
        self.separador = ttk.Separator(orient="vertical") #Separador central
        self.separador.grid(row=0,column=4,sticky="ns",rowspan=6,padx=10,pady=10)
        self.separadorPlanetas = ttk.Separator(orient="vertical") #separador de planetas
        self.separadorPlanetas.grid(row=0,column=2,sticky="ns",rowspan=4,padx=10,pady=10)
        self.separadorAliens = ttk.Separator(orient="vertical") #separador de aliens
        self.separadorAliens.grid(row=0,column=7,sticky="ns",rowspan=4,padx=10,pady=10)
        
    #
    # METODOS REFERENTES A LOS PLANETAS
    #
    # MOSTRAR PLANETAS EN LA TABLA
    def mostrarPlanetas(self):
        """Muestra los planetas de la base de datos en la tabla de la interfaz
        """
        del self.tabla_planetas # Elimina la tabla para vaciarla y vuelve a crearla, actualizandola en el proceso
        self.tabla_planetas = ttk.Treeview(columns=("ID","Planeta","Galaxia"),show="headings")
        self.tabla_planetas.heading("ID",text="ID")
        self.tabla_planetas.heading("Planeta",text="PLANETA")
        self.tabla_planetas.heading("Galaxia",text="GALAXIA")
        self.tabla_planetas.grid(row=4,column=0,columnspan=4,padx=10,pady=10)
        for i in range(len(self.planetaMain.verPlanetas())):
            self.tabla_planetas.insert("","end",values=self.planetaMain.verPlanetas()[i]) #Aquí se pasa todo lo de la base de datos a la tabla
    
    # GUARDAR PLANETA EN LA BASE DE DATOS
    def guardarPlaneta(self):
        """Guarda el planeta de los campos de texto en la database
        """
        planeta = Planeta(self.varNombrePlaneta.get(),self.varGalaxia.get())
        planeta.guardarPlaneta()
        self.limpiarPlaneta()
        self.mostrarPlanetas()
    
    # LIMPIAR CAMPOS DE TEXTO DE PLANETAS
    def limpiarPlaneta(self):
        """limpia los campos de texto de planetas"""
        self.varNombrePlaneta.set("")
        self.varGalaxia.set("")
        
    # ACTUALIZAR PLANETA EN LA BASE DE DATOS
    def editarPlaneta(self):
        """Actualiza el planeta de la id"""
        planeta = Planeta(self.varNombrePlaneta.get(),self.varGalaxia.get())
        planeta.editarPlanetas(self.varIdPlaneta.get())
        self.mostrarPlanetas()
        self.varIdPlaneta.set(0)
    
    # ELIMINAR PLANETAS DE LA BASE DE DATOS
    def eliminarPlaneta(self):
        """Elimina el planeta de la id""" 
        opcion = messagebox.askokcancel("Advertencia","Si elimina este planeta, eliminará todos los aliens que pertenezcan a él.")
        if opcion:
            self.planetaMain.eliminarPlaneta(self.varIdPlaneta.get())
            self.alienMain.eliminarAlienDePlaneta(self.varIdPlaneta.get())
            self.mostrarPlanetas()
            self.mostrarAliens()
            self.varIdPlaneta.set(0)
    
    #
    # METODOS REFERENTES A LOS ALIENS
    #    
    # MOSTRAR ALIENS EN LA TABLA
    def mostrarAliens(self):
        """carga los aliens de la base de datos a la tabla de la interfaz"""
        del self.tabla_aliens # Elimina la tabla para vaciarla y vuelve a crearla, actualizandola en el proceso
        self.tabla_aliens = ttk.Treeview(columns=("ID","Alien","IdPlaneta"),show="headings")
        self.tabla_aliens.heading("ID",text="ID")
        self.tabla_aliens.heading("Alien",text="ALIEN")
        self.tabla_aliens.heading("IdPlaneta",text="ID PLANETA")
        self.tabla_aliens.grid(row=4,column=5,columnspan=4,padx=10,pady=10)
        for i in range(len(self.alienMain.verAliens())):
            self.tabla_aliens.insert("","end",values=self.alienMain.verAliens()[i]) #Aquí se pasa todo lo de la base de datos a la tabla
            
    # GUARDAR ALIEN EN LA BASE DE DATOS
    def guardarAlien(self):
        """guarda el alien de los campos de texto en la base de datos"""
        alien = Alien(self.varNombreAlien.get(),self.varIdPlanetaAlien.get())
        alien.guardarAlien(self.planetaMain.leerIds())
        self.limpiarAlien()
        self.mostrarAliens()
    
    # LIMPIAR CAMPOS DE TEXTO DE ALIENS
    def limpiarAlien(self):
        """limpia los campos de texto de aliens"""
        self.varNombreAlien.set("")
        self.varIdPlanetaAlien.set(0)
        
    # ACTUALIZAR ALIENS EN LA BASE DE DATOS
    def editarAlien(self):
        """actualiza el alien de la id"""
        alien = Alien(self.varNombreAlien.get(),self.varIdPlanetaAlien.get())
        alien.editarAlien(self.varIdAlien.get(),self.planetaMain.leerIds())
        self.mostrarAliens()
        self.varIdAlien.set(0)
    
    # ELIMINAR ALIENS DE LA BASE DE DATOS
    def eliminarAlien(self):
        "elimina el alien de la id"
        self.alienMain.eliminarAlien(self.varIdAlien.get())
        self.mostrarAliens()
        self.varIdAlien.set(0)
    
    # METODO PARA CAMBIAR EL TEMA
    def cambiarTema(self):
        """cambia el tema de la ventana"""
        sv_ttk.toggle_theme(self.ventana)
        