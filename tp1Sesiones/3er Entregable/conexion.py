import sqlite3
from tkinter import messagebox
import json

class Conexion:
    def __init__(self):
        """Crea el objeto conexion"""
        self.ruta = "./tp1Sesiones/3er Entregable/planetas_y_aliens.sqlite3"
        self.conexion = sqlite3.connect(self.ruta) #CONECTA LA BASE DE DATOS
        self.cursor = self.conexion.cursor() # CREA EL CURSOR
        with open("./tp1Sesiones/3er Entregable/queries.json","r") as archivo:
            self.queries = json.loads(archivo.read()) #CARGA LOS QUERIES DEL ARCHIVO JSON
        
    def crearTabla(self): #SE USÓ PARA CREAR AMBAS TABLAS
        """Crea las tablas"""
        self.cursor.execute(self.queries["CrearTabla"].format("Planetas",self.queries["ColumnasPlanetas"]))
        self.cursor.execute(self.queries["CrearTabla"].format("Aliens",self.queries["ColumnasAliens"]))