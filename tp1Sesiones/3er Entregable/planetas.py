from conexion import Conexion
from tkinter import messagebox

class Planeta(Conexion):
    def __init__(self,nombrePlaneta="",galaxia=""):
        """Crear objeto Planeta

        Args:
            nombrePlaneta (String): Nombre del planeta
            galaxia (String): Galaxia en la que se encuentra el planeta
        """
        super().__init__()
        self.id = None
        self.nombrePlaneta = nombrePlaneta
        self.galaxia = galaxia
        
    # GUARDAR PLANETAS EN LA BASE DE DATOS
    def guardarPlaneta(self):
        """Guarda el planeta en la database
        """
        if self.nombrePlaneta!="" and self.galaxia!="":
            self.cursor.executemany(self.queries["InsertarDatos"].format("Planetas","?,?"),[(self.nombrePlaneta,self.galaxia)])
            self.conexion.commit()
            messagebox.showinfo("Info","El Planeta se ha guardado con éxito.")
        else:
            messagebox.showerror("Error","El campo Nombre o el campo Galaxia están vacíos.")
            
    # VER PLANETAS EN LA BASE DE DATOS
    def verPlanetas(self):
        """Retorna los planetas en la base de datos

        Returns:
            List: planetas de la base de datos
        """
        if self.nombrePlaneta!="" and self.galaxia!="":
            self.cursor.execute(self.queries["LeerDatos"].format("Planetas"))
            return self.cursor.fetchall()
        else:
            print("Error en ver planetas")
            
    # ACTUALIZAR PLANETAS DE LA BASE DE DATOS
    def editarPlanetas(self,id=0):
        """Actualiza el planeta en cuestion en la base de datos

        Args:
            id (int): id del planeta a actualizar
        """
        auxId = False
        self.cursor.execute(self.queries["LeerId"].format("Planetas"))
        for i in self.cursor.fetchall():
            if id == i[0]:
                auxId=True
        if auxId:
            if self.nombrePlaneta!="" and self.galaxia!="":
                self.cursor.execute(self.queries["ActualizarDatos"].format("Planetas","Planeta",self.nombrePlaneta,"Id",id))
                self.cursor.execute(self.queries["ActualizarDatos"].format("Planetas","Galaxia",self.galaxia,"Id",id))
                self.conexion.commit()
                messagebox.showinfo("Info","El Planeta se ha actualizado con éxito.")
            else:
                messagebox.showerror("Error","El campo Nombre o el campo Galaxia están vacíos.")
        else:
            messagebox.showerror("Error","El ID seleccionado no existe.")
        
    # ELIMINAR PLANETAS DE LA BASE DE DATOS    
    def eliminarPlaneta(self,id=0):
        """Elimina el planeta de la id

        Args:
            id (int): elimina el planeta con esta id en la base de datos
        """
        auxId = False
        self.cursor.execute(self.queries["LeerId"].format("Planetas"))
        for i in self.cursor.fetchall():
            if id == i[0]:
                auxId=True
        if auxId:
            self.cursor.execute(self.queries["EliminarDatos"].format("Planetas","Id",id))
            self.conexion.commit()
            messagebox.showinfo("Info","El Planeta se ha eliminado con éxito.")
        else:
            messagebox.showerror("Error","El ID seleccionado no existe.")
            
    # LEE LAS IDS DE LOS PLANETAS DE LA BASE DE DATOS
    def leerIds(self):
        """Retorna los ids de los planetas registrados

        Returns:
            list: ids de planetas
        """
        self.cursor.execute(self.queries["LeerId"].format("Planetas"))
        return self.cursor.fetchall()