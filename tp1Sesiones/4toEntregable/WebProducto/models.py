from django.db import models

class Producto(models.Model):
    ID = models.IntegerField(primary_key=True)
    NOMBRE = models.TextField(max_length=30)
    DESCRIPCION = models.TextField(max_length=200)
    CATEGORIA = models.TextField(max_length=30)
    PRECIO = models.IntegerField()
    IMAGEN = models.TextField()


# Producto.objects.create(NOMBRE="RX 6600",DESCRIPCION="La RX 6600 es una excelente opción para los jugadores que buscan una tarjeta gráfica que les permita disfrutar de los juegos más recientes en 1080p a 60 FPS o más",CATEGORIA="Tarjeta Grafica",PRECIO=200,IMAGEN="img/productos/producto1.jpg")
# Producto.objects.create(NOMBRE="RTX 3060",DESCRIPCION="La RTX 3060 cuenta con una GPU GA106 de 3584 núcleos, 12 GB de memoria GDDR6 y un ancho de banda de memoria de 360 GB/s",CATEGORIA="Tarjeta Grafica",PRECIO=500,IMAGEN="img/productos/producto2.jpg")
# Producto.objects.create(NOMBRE="RTX 1050",DESCRIPCION="La GTX 1050 cuenta con una GPU GM107 de 640 núcleos, 2 GB de memoria GDDR5 y un ancho de banda de memoria de 128 GB/s. Esto le permite ofrecer un rendimiento excelente en juegos como Fortnite, Call of Duty: Warzone y Apex Legends",CATEGORIA="Tarjeta Grafica",PRECIO=100,IMAGEN="img/productos/producto3.png")
# Producto.objects.create(NOMBRE="RTX 1050",DESCRIPCION="La GTX 1080 fue una de las tarjetas gráficas más potentes del mercado cuando se lanzó en 2016, y sigue siendo una excelente opción para los jugadores que buscan una experiencia de juego fluida en resoluciones 1440p y 4K.",CATEGORIA="Tarjeta Grafica",PRECIO=300,IMAGEN="img/productos/producto4.png")

# Producto.objects.create(NOMBRE="Asus B450",DESCRIPCION="La Asus B450 es una placa base potente y versátil que es una excelente opción para los usuarios que buscan construir un PC de alto rendimiento a un precio asequible.",CATEGORIA="Placa Madre",PRECIO=200,IMAGEN="img/productos/producto5.jpg")
# Producto.objects.create(NOMBRE="A-320",DESCRIPCION="La A320 es una buena elección para quienes buscan una placa base asequible y fiable para su PC basado en AMD Ryzen.",CATEGORIA="Placa Madre",PRECIO=150,IMAGEN="img/productos/producto6.png")
# Producto.objects.create(NOMBRE="MSI B550",DESCRIPCION="La MSI B550 es una excelente elección para quienes buscan una placa base potente y equilibrada que ofrezca un rendimiento excelente a un precio asequible.",CATEGORIA="Placa Madre",PRECIO=300,IMAGEN="img/productos/producto7.jpg")
# Producto.objects.create(NOMBRE="Aorus X670",DESCRIPCION="La Aorus X670 es una excelente elección para quienes buscan una placa base potente y versátil que ofrezca un rendimiento de primer nivel.",CATEGORIA="Placa Madre",PRECIO=600,IMAGEN="img/productos/producto8.jpg")