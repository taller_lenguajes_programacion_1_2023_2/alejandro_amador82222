from django.shortcuts import render
from .models import Producto

def paginaProductos(request,categoria):
    nombre = request.session.get('nombre', None)
    user = request.session.get('user', None)
    return render (request, 'products.html', {
        "productos": list(Producto.objects.filter(CATEGORIA = categoria)),
        "categoria": categoria,
        "nombre":nombre,
        "user":user
    })

def paginaCategory(request):
    nombre = request.session.get('nombre', None)
    user = request.session.get('user', None)
    return render(request, 'category.html', {
        "nombre":nombre,
        "user":user
    })
    
def paginaDetalle(request,producto):
    nombre = request.session.get('nombre', None)
    user = request.session.get('user', None)
    return render (request, 'detalle.html', {
        "producto" : Producto.objects.get(ID = producto),
        "nombre":nombre,
        "user":user
    })