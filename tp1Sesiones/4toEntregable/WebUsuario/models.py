from django.db import models

class Cliente(models.Model):
    NOMBRE = models.CharField(max_length=30)
    PRIMER_APELLIDO = models.CharField(max_length=30)
    SEGUNDO_APELLIDO = models.CharField(max_length=30)
    CORREO = models.CharField(max_length=20)
    USUARIO = models.CharField(max_length=30)
    CONTRASENA = models.CharField(max_length=30)