from django.shortcuts import render,redirect
from .models import Cliente

# Create your views here.
def paginaIndex(request):
    return render(request,'index.html')

def paginaLogin(request):
    msg = ""
    if request.method == "POST":
        usuario = request.POST['usuario']
        contrasena = request.POST['contrasena']
        
        try:
            validar = Cliente.objects.get(USUARIO=usuario)
            if validar.CONTRASENA == contrasena:
                request.session['nombre'] = validar.NOMBRE +" "+ validar.PRIMER_APELLIDO +" "+ validar.SEGUNDO_APELLIDO
                request.session['user'] = validar.USUARIO
                return redirect('/categories')
            else: 
                msg = "La contraseña no es correcta."
        except:
            msg="El Usuario no se encuentra Registrado."
        
    return render(request,'login.html', {
        "msgLogin":msg
    })

def paginaRegister(request):
    msgRegister=""
    if request.method == 'POST':
        nombre = request.POST['nombre']
        primerApellido = request.POST['pApellido']
        segundoApellido = request.POST['sApellido']
        correo = request.POST['correo']
        usuario = request.POST['usuario']
        contrasena = request.POST['contrasena']
        contrasena1 = request.POST['contrasena1']
        
        if contrasena1 == contrasena:
            try:
                usuarioExistente = Cliente.objects.get(USUARIO = usuario)
                msgRegister = "El usuario ya está registrado."
            except:
                nuevocliente = Cliente.objects.create(NOMBRE=nombre,PRIMER_APELLIDO = primerApellido, SEGUNDO_APELLIDO = segundoApellido, CORREO =correo, USUARIO = usuario, CONTRASENA=contrasena)
                return redirect('/login')
        else:
            msgRegister = "Los campos de contraseñas deben coincidir."
        
    return render(request,'register.html', {
        "msgRegister":msgRegister
    })