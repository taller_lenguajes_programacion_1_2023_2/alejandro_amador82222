function comprar() {
    Swal.fire({
        title: "¿Comprar?",
        text: "El coste del producto será descontado de tu saldo automaticamente",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si, comprar"
      }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire({
                title: "Comprado",
                text: "El producto será enviado a tu residencia.",
                icon: "success"
            }).then((result) => {
                window.location.href = '/categories';
            });
        }
      });
}

function cierreSesion() {
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: "btn btn-success",
            cancelButton: "btn btn-danger"
        },
        buttonsStyling: false
    });
    swalWithBootstrapButtons.fire({
        title: "¿Cerrar Sesión?",
        text: "",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, cerrar sesión",
        cancelButtonText: "No, cancela",
        reverseButtons: false
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = '/';
        }
    });
}