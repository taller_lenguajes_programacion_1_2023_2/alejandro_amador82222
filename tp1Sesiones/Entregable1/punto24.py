#Entregable 1 - Alejandro Amador Ruiz - 1007480914 - Ejercicios terminados en 4

#24) Escribe un programa que determine si un elemento dado está presente en una lista o no

aar_listaPares=[2,4,6,8,10,12,14,16,18,20]

aar_numero=int(input("Ingrese un numero para saber si está en la lista: "))

if aar_numero in aar_listaPares:
    print("El numero está en la lista.")
else:
    print("El numero NO está en la lista.")    