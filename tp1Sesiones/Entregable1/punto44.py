#Entregable 1 - Alejandro Amador Ruiz - 1007480914 - Ejercicios terminados en 4

#44) Simula un archivo CSV productos.csv, escribe un programa que filtre y muestre solo aquellos productos cuyo precio sea mayor a 50
import pandas as pd
import statistics as st
aar_archivo = pd.read_csv("./tp1Sesiones/Entregable1/44.csv",sep=",")
aar_df = pd.DataFrame(aar_archivo)
print(aar_df[aar_df["Precio"]>50])