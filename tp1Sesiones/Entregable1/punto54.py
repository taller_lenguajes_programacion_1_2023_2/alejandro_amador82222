#Entregable 1 - Alejandro Amador Ruiz - 1007480914 - Ejercicios terminados en 4

#54) Escribe un programa que divida un archivo CSV grande en múltiples archivos más pequeños
#    con un número determinado de filas

import pandas as pd
import statistics as st
aar_archivo = pd.read_csv("./tp1Sesiones/Entregable1/54.csv",sep=",")
aar_df = pd.DataFrame(aar_archivo)
print(aar_df)
aar_part = int(input("Ingrese la cantidad de filas que tendrá cada particion: "))
aar_listaDF = []
for i in range(0,len(aar_df),aar_part):
    aar_df_aux = aar_df.iloc[i:i + aar_part]
    aar_listaDF.append(aar_df_aux)
    
print(aar_listaDF)