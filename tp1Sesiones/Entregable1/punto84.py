#Entregable 1 - Alejandro Amador Ruiz - 1007480914 - Ejercicios terminados en 4

#84) Escribe un programa que lea un archivo Excel y rellene las celdas vacias
#    de una columna con el valor promedio de esa columna

import pandas as pd
aar_archivo = pd.read_excel("./tp1Sesiones/Entregable1/84.xlsx")

aar_df = pd.DataFrame(aar_archivo)

aar_promedio = aar_df["Edad"].dropna().mean()

aar_df["Edad"].fillna(aar_promedio,inplace=True)

print(aar_df)