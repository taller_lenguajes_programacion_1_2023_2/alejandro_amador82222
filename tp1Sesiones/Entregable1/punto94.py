#Entregable 1 - Alejandro Amador Ruiz - 1007480914 - Ejercicios terminados en 4

#94) Simula un archivo Excel con fechas, Escribe un programa que extraiga el mes
#    y el año en columnas separadas  

import pandas as pd

aar_df = pd.DataFrame(pd.read_excel("./tp1Sesiones/Entregable1/94.xlsx"))

meses = [mes for mes in aar_df["Fechas"].dt.month]
años = [año for año in aar_df["Fechas"].dt.year]

aar_df["mes"] = meses
aar_df["año"] = años

print(aar_df)
