from Vuelo import Vuelo
from tp1Sesiones.Parcial1.Pasajero import Pasajero
from Aerolinea import Aerolinea

class Aerolinea(Vuelo):
    def __init__(self, id=0, nombre="", capacidad=0, autonomia=0, numeroSerie="", añoFabricacion=0, aerolinea=Aerolinea, fechaSalida=0, fechaLlegada=0, origen="", destino="", pasajeros=Pasajero, nombreAerolinea="",paisOrigen="", fundacion = 0, flota=0, destinos = [], codigoIATA=""):
        super().__init__(id, nombre, capacidad, autonomia, numeroSerie, añoFabricacion, aerolinea, fechaSalida, fechaLlegada, origen, destino, pasajeros)
        self.id = id
        self.nombreAerolinea = nombreAerolinea
        self.paisOrigen = paisOrigen
        self.fundacion = fundacion
        self.flota = flota
        self.destinos = destinos
        self.codigoIATA = codigoIATA
        
    def IniciarTabla(self):
        """
            Creación de las tablas con respectiva columnas
        """
        self.CrearTabla("Aerolinea", "ColumnasAerolinea")    
    
    # Getters
    def getId(self):
        return self.id

    def getNombreAerolinea(self):
        return self.nombreAerolinea

    def getPaisOrigen(self):
        return self.paisOrigen

    def getFundacion(self):
        return self.fundacion

    def getFlota(self):
        return self.flota

    def getDestinos(self):
        return self.destinos

    def getCodigoIATA(self):
        return self.codigoIATA

    # Setters
    def setId(self, id):
        self.id = id

    def setNombreAerolinea(self, nombreAerolinea):
        self.nombreAerolinea = nombreAerolinea

    def setPaisOrigen(self, paisOrigen):
        self.paisOrigen = paisOrigen

    def setFundacion(self, fundacion):
        self.fundacion = fundacion

    def setFlota(self, flota):
        self.flota = flota

    def setDestinos(self, destinos):
        self.destinos = destinos

    def setCodigoIATA(self, codigoIATA):
        self.codigoIATA = codigoIATA
        
    def __str__(self):
        return f"Aerolínea: {self.nombreAerolinea}\n" \
            f"ID: {self.id}\n" \
            f"País de origen: {self.paisOrigen}\n" \
            f"Fecha de fundación: {self.fundacion}\n" \
            f"Flota: {self.flota}\n" \
            f"Destinos: {self.destinos}\n" \
            f"Código IATA: {self.codigoIATA}"