from Aerolinea import Aerolinea
from conexion import DataBase

class Avion(DataBase):
    def __init__(self,id,modelo,capacidad,autonomia,numeroSerie,añoFabricacion,aerolinea=Aerolinea):
        self.id = id
        self.modelo=modelo
        self.capacidad = capacidad
        self.autonomia = autonomia
        self.numeroSerie = numeroSerie
        self.añoFabricacion = añoFabricacion
        
    # Getters
    def getId(self):
        return self.id

    def getModelo(self):
        return self.modelo

    def getCapacidad(self):
        return self.capacidad

    def getAutonomia(self):
        return self.autonomia

    def getNumeroSerie(self):
        return self.numeroSerie

    def getAñoFabricacion(self):
        return self.añoFabricacion

    # Setters
    def setId(self, id):
        self.id = id

    def setModelo(self, modelo):
        self.modelo = modelo

    def setCapacidad(self, capacidad):
        self.capacidad = capacidad

    def setAutonomia(self, autonomia):
        self.autonomia = autonomia

    def setNumeroSerie(self, numeroSerie):
        self.numeroSerie = numeroSerie

    def setAñoFabricacion(self, añoFabricacion):
        self.añoFabricacion = añoFabricacion
        
    def __str__(self):
        return f"Avión: {self.modelo}\n" \
            f"ID: {self.id}\n" \
            f"Capacidad: {self.capacidad}\n" \
            f"Autonomia: {self.autonomia}\n" \
            f"Número de serie: {self.numeroSerie}\n" \
            f"Año de fabricación: {self.añoFabricacion}"