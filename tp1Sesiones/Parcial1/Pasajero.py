from Vuelo import Vuelo
from tp1Sesiones.Parcial1.Aerolinea import Aerolinea
from Pasajero import Pasajero 

class Pasajero(Vuelo):
    def __init__(self, id=0, nombre="", capacidad=0, autonomia=0, numeroSerie="", añoFabricacion=0, aerolinea=Aerolinea, fechaSalida=0, fechaLlegada=0, origen="", destino="", pasajeros=Pasajero,nombrePasajero="",apellido="",dni="",nacionalidad="",fechaNacimiento=0,pasaporte=""):
        super().__init__(id, nombre, capacidad, autonomia, numeroSerie, añoFabricacion, aerolinea, fechaSalida, fechaLlegada, origen, destino, pasajeros)
        self.id = id
        self.nombrePasajero=nombrePasajero
        self.apellido = apellido
        self.dni = dni
        self.nacionalidad = nacionalidad
        self.fechaNacimiento = fechaNacimiento
        self.pasaporte = pasaporte
        
        
    def IniciarTabla(self):
        """
            Creación de las tablas con respectiva columnas
        """
        self.CrearTabla("Pasajero", "ColumnasPasajero")
        self.CrearTabla("Avion","ColumnasAvion")
        self.CrearTabla("Vuelo","ColumnasVuelo")
    
    # Getters
    def getId(self):
        return self.id

    def getNombrePasajero(self):
        return self.nombrePasajero

    def getApellido(self):
        return self.apellido

    def getDni(self):
        return self.dni

    def getNacionalidad(self):
        return self.nacionalidad

    def getFechaNacimiento(self):
        return self.fechaNacimiento
    
    def getPasaporte(self):
        return self.pasaporte

    # Setters
    def setId(self, id):
        self.id = id

    def setNombrePasajero(self, nombrePasajero):
        self.nombrePasajero = nombrePasajero

    def setApellido(self, apellido):
        self.apellido = apellido

    def setDni(self, dni):
        self.dni = dni

    def setNacionalidad(self, nacionalidad):
        self.nacionalidad = nacionalidad

    def setFechaNacimiento(self, fechaNacimiento):
        self.fechaNacimiento = fechaNacimiento
        
    def setPasaporte(self, pasaporte):
        self.pasaporte = pasaporte
        
    def __str__(self):
        return f"ID: {self.id}\n" \
            f"Nombre: {self.nombrePasajero}\n" \
            f"Apellido: {self.apellido}\n" \
            f"DNI: {self.dni}\n" \
            f"Nacionalidad: {self.nacionalidad}\n" \
            f"Fecha de nacimiento: {self.fechaNacimiento}" \
            f"Pasaporte: {self.pasaporte}"