from Avion import Avion
from Pasajero import Pasajero
from Aerolinea import Aerolinea

class Vuelo(Avion):
    def __init__(self, id=0, nombre="", capacidad=0, autonomia=0, numeroSerie="", añoFabricacion=0, aerolinea=Aerolinea, fechaSalida=0, fechaLlegada=0, origen="", destino="", pasajeros=Pasajero):
        super().__init__(id, nombre, capacidad, autonomia, numeroSerie, añoFabricacion,aerolinea)
        self.id=id
        self.fechaSalida = fechaSalida
        self.fechaLlegada = fechaLlegada
        self.origen = origen
        self.destino = destino
        self.pasajeros = pasajeros
        
    # Getters
    def getId(self):
        return self.id

    def getFechaSalida(self):
        return self.fechaSalida

    def getFechaLlegada(self):
        return self.fechaLlegada

    def getOrigen(self):
        return self.origen

    def getDestino(self):
        return self.destino

    def getPasajeros(self):
        return list(self.pasajeros)

    # Setters
    def setId(self, id):
        self.id = id

    def setFechaSalida(self, fechaSalida):
        self.fechaSalida = fechaSalida

    def setFechaLlegada(self, fechaLlegada):
        self.fechaLlegada = fechaLlegada

    def setOrigen(self, origen):
        self.origen = origen

    def setDestino(self, destino):
        self.destino = destino

    def setPasajeros(self, pasajeros):
        self.pasajeros = pasajeros
        
    def __str__(self):
        return f"Vuelo:\n" \
            f"ID: {self.id}\n" \
            f"Fecha de salida: {self.fechaSalida}\n" \
            f"Fecha de llegada: {self.fechaLlegada}\n" \
            f"Origen: {self.origen}\n" \
            f"Destino: {self.destino}\n" \
            f"Pasajeros: {self.pasajeros}"