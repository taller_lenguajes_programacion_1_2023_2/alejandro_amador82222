import sqlite3
import json

# Query : Consultar en base de datos, eliminar, agregar, cambiar, consultar, etc. 

#esto empieza a crear la tabla           llave primaria    autoincrementada         valor unico       campo obligatorio
#pointer.execute("CREATE TABLE Personas (Id INTEGER PRIMARY KEY AUTOINCREMENT, Cedula INTEGER UNIQUE, Nombre VARCHAR(20) NOT NULL, Apellido VARCHAR(30) NOT NULL)")

class DataBase:
    def __init__(self):
        self.database = sqlite3.connect("C:/Users/ASUS/Desktop/alejandro_amador82222/src/tp1_sesiones/SQL/database/database.sqlite")
        self.pointer = self.database.cursor()    

        with open("C:/Users/ASUS/Desktop/alejandro_amador82222/src/tp1_sesiones/SQL/Query.json","r") as consultas:
            self.queries = json.load(consultas)

    def CrearTabla(self, nombreTabla="",Columnas=""):
        if nombreTabla!="" and Columnas!="":
            info = self.queries["CrearTabla"].format(nombreTabla,self.queries[Columnas])
            print(info)
            self.pointer.execute(info)
            self.database.commit()
            print(f"Base de datos, tabla {nombreTabla} se ha creado correctamente.")
    
    def InsertarDato(self,tabla="",interrogantes="",valores=""):
        if tabla!="" and valores!="":
            info = self.queries["InsertarDato"].format(tabla,interrogantes)
            self.pointer.executemany(info,valores)
            self.database.commit()
            print("Datos insertados correctamente")