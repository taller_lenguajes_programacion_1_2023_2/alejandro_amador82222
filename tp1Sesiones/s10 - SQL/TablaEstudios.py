from SQLMain import DataBase

class tablaEstudios(DataBase):
    def __init__(self):
        super().__init__()
        
    def CrearTablaEstudios(self):
        self.CrearTabla("Estudios","ColumnasEstudios")
        
    def AgregarDato(self,carrera,fechaI,fechaF):
        datos = [
            (carrera,fechaI,fechaF)
        ]
        self.InsertarDato("Estudios","?,?,?",datos)