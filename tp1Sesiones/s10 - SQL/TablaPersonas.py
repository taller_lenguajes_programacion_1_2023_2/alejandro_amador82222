from SQLMain import DataBase

class tablaPersona(DataBase):
    def __init__(self):
        super().__init__()
        
    def CrearTablaPersonas(self):
        self.CrearTabla("Personas","ColumnasPersonas")
        
    def AgregarDato(self,cedula,nombre,apellido):
        datos = [
            (cedula,nombre,apellido)
        ]
        self.InsertarDato("Personas","?,?,?",datos)