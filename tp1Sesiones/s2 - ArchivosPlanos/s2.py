# VARIABLES Y TIPOS DE VARIABLES

nombre = "Hello World"
print(type(nombre))

entero = 23
print(type(entero))

flotante = 14.3
print(type(flotante))

booleano = True
print(type(booleano))

# ESTRUCTURAS DE DATOS

#Lists
lista = [1,4,67,2,3]
#metodos: sort, append, reverse, clear, copy, cound, extend, index, insert, pop, remove

#Tuples
tupl = (1,5,2,3,6,12)
#metodos: count, index

#sets
st = {1,7,3,46,2}
#metodos: add, clear, copy, difference, discard, intersection, isdisjoint, issubset, issuperset, pop, remove, union, update

#dictionaries
dicc = {
    "nombre":"Alejo",
    "apellido":"Amador",
    "edad":19
}
#metodos: clear, copy, fromkeys, get, items, keys, popitem, setdefault, update, values

print(lista[2],tupl[-1],st,dicc["nombre"])

#   --------------- ARCHIVOS PLANOS ----------------

# SE USA "r" PARA LEER
#with open("./src/tp1_sesiones/static/txt/sesion2.txt","r") as archivo:
#    contenido = archivo.read()
#print(contenido)

# SE USA "w" PARA ESCRIBIR
#datos = "Esto es un ejemplo."
#with open("./src/tp1_sesiones/static/txt/sesion2_copy.txt", "r+") as archivo:
#    archivo.write("hola mundo",25)
#    contenido=archivo.read()
#print(contenido)

datos = "Esto es un ejemplo 2."
with open("./src/tp1_sesiones/static/txt/sesion2_copy.txt", "a") as archivo:
    archivo.write("\n{}".format(datos))

with open("./src/tp1_sesiones/static/txt/sesion2_copy.txt", "r") as archivo:
    contenido=archivo.read()
print(contenido)