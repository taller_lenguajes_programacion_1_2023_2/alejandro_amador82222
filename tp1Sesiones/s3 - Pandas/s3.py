# SESION 3 - IF, WHILE, Y ARCHIVOS

with open("./src/tp1_sesiones/static/txt/sesion3.txt","r+") as archivo:
    listaDeLineas = archivo.readlines() #copiar las lineas del documento como posiciones de una lista
    st = listaDeLineas[0].split("\n") 
    print(st)
    print(listaDeLineas)
    
    Tlineas=0
    Cont=1
    Found=False
    for frase in listaDeLineas:
        if frase=="Esta es la linea 7\n":
            Found=True
        if Found==False:
            Cont += 1
        Tlineas +=1
    print("Total de lineas en el documento: ",Tlineas)
    if Found==True:
        print("La frase esta en el conjunto, en el renglon: ",Cont)
    else:
        print("La frase no esta en el conjunto.")  
        
# IMPORTACION DE LIBRERIAS   
#para poder usar pandas se debe usar en la terminal: pip install pandas
import pandas as pd

archivo = pd.read_csv("./src/tp1_sesiones/static/txt/sesion2.txt",sep=";")
#Se crea un Dataframe (hoja de caculo), el segundo parametro indica en que caracter se considera una fila nueva

archivo["Referencias"] = "https://pandas.pydata.org/docs/user_guide/index.html#user-guide"
#Se agrega una columna con una url en todas las filas
print("Hasta ahora todo joya")

#para poder exportar a excel se debe usar en la terminal: pip install openpyxl
archivo.to_excel(r'C:/Users/aleam/OneDrive/Documentos/Universidad/Semestre3/tallerDeLenguajes/AlejandroAmadorRuiz/alejandro_amador82222/src/tp1_sesiones/static/xlsx/Prueba.xlsx',index=False)
#Se exporta en un archivo de Excel

print("Felicitaciones Alejo, haz logrado solucionar los mil y un errores que te estaba dando :b")