# DATAFRAME CON PANDAS URL

#Importamos pandas
import pandas as pd

# Importar la tabla HTML
tabla = pd.read_html("https://en.wikipedia.org/wiki/List_of_countries_by_GDP_(nominal)_per_capita")
paises = tabla[1]

paises.rename(columns={ # Cambiar el nombre de los encabezados, en la posición 1 está el DataFrame
    "Country/Territory": "País",
    "UN Region": "Región",
    "IMF[4][5]": "FMI",
    "Estimate": "Estimación",
    "Year": "Año",
    "World Bank[6]": "Banco Mundial",
    "United Nations[7]": "Naciones Unidas",
}, inplace=True)

paises = paises.drop(0, inplace=False) # Borrar la fila 0 que se genero vacía
paises.fillna(method="ffill", inplace=True) # subir los demas elementos

#paises.to_excel("./src/tp1_sesiones/static/xlsx/countries.xlsx")

monedas = pd.read_html("https://www.x-rates.com/table/?from=USD&amount=1")
monedas = monedas[1]

monedas.rename(columns= {
    "US Dollar":"Moneda",
    "1.00 USD":"1 USD en Moneda",
    "inv. 1.00 USD":"1 Moneda en USD"
},inplace=True)

monedas.to_excel("./src/tp1_sesiones/static/xlsx/Monedas.xlsx")
