# Taller 1
#1
print("Hola mundo")

#2
variableString="hola"
variableInt = 77
variableFloat = 4.5

#3
print(type(variableString)) #metodo type()
print(type(variableInt))
print(type(variableFloat))

#4
suma,resta,multiplicacion,division = 3+4,6-3,20*8,25/5 # +,-,*,/,//,%,**
print(f'Suma: {suma}, Resta: {resta}, Multiplicacion: {multiplicacion}, Division: {division}')

#5
variableIntAString = str(variableInt)
print(type(variableIntAString))

#6 
#entrada = input("Ingrese su edad:")

#7,8,9
if suma<resta:
    print("Suma es menor a resta")
elif suma>resta:
    print("Suma es mayor a resta")
else:
    print('Suma es igual a resta')
    
#10
lista = [1,6,32,7,3,2,7]
for i in lista:
    print(i,end="")
print("\n------------------------------------")
for i in range(0,6):
    print (i,end="")
print("\n------------------------------------")

#11
i=0
while i<10:
    print(i,end=" ")
    i=i+1

#12
while i<10:
    if i==0 or i==5:
        continue
    elif i==8:
        break
    print(i,end=" ")    
    i=i+1
    
#13 y 17
listaEjemplo = []
lista.append(909) # remove, insert, pop, clear, extend, index, etc

#14
tupla = (1,34,5,67,23)
#solo tiene count e index

#15
conjunto = {1,2,3,4,5}
#es como un conjunto de la vida real, puede tener union,interseccion,diferencia,subconjunto, etc...

#16
diccionario = {
    'nombre':'Alejo',
    'apellido':'Amador'
}

#18
with open("./src/tp1_sesiones/static/txt/prueba.txt","r") as archivo:
    print(archivo.read())
    
#19
with open("./src/tp1_sesiones/static/txt/prueba.txt","w") as archivo:
    archivo.write("hola mundo 2")
    
#20
#r:leer,w:escribir,a:añadir,rb:leer imagenes, wb:escribir imagenes


#21
import json #importamos json para trabajar con archivos json
Datos={
    "Nombre":"Alejo",
    "Edad":"19",
    "Documento":"2309856"    
}
with open("./src/tp1_sesiones/static/json/taller1.json","w") as js:
    json.dump(Datos, js) #dump es para guardar los datos de un json
    
#22
with open("./src/tp1_sesiones/static/json/taller1.json","r") as js:
    Archivo=json.load(js) #load es para cargar un archivo.json
    print(Archivo)
    
    
#23
import pandas as pd
Documento = pd.read_csv("./src/tp1_sesiones/static/txt/taller1.txt",sep=";") 
Filtrar=Documento[Documento["Fecha"]=="30/08/2023"]
print(Filtrar.to_string(index=False))

#24
import numpy as np #libreria para trabajar funciones basicas de matematica

Numeros=[1,2,3,4,5]
print(f"Numeros: {Numeros}")
Sum=np.sum(Numeros)
print(f"Sum: {Sum}")
Mean=np.mean(Numeros)
print(f"Mean: {Mean}")
Max=np.max(Numeros)
print(f"Max: {Max}")
Min=np.min(Numeros)
print(f"Min: {Min}")

#25
df=pd.DataFrame({
  "Edad": [18, 25, 30, 40],
  "Genero": ["Hombre", "Hombre", "Mujer", "Mujer"]
})
print(f"Iloc:\n{df.iloc[0]}") #iloc busca la fila
print("Loc:\n",df.loc[df["Edad"]==30]) #loc busca una casilla en concreto
print("Casilla:\n",df.loc[df["Edad"]==25,"Genero"])

#26
df=pd.DataFrame({
    "nombre": ["Juan", "Pedro", "María", "José"],
    "edad": [20, 25, 30, 35]
})
df.groupby("nombre")
for name in df.groupby("nombre"):
    print(name)

#27
df1=pd.DataFrame({
    "A": [1, 2, 3],
    "B": [4, 5, 6]
})
df2=pd.DataFrame({
    "A": [1, 2, 3],
    "C": [7, 8, 9],
    "D": [10, 11, 12]
})

concat=pd.concat([df1, df2], axis=1)
print(f"Concat:\n{concat}")

merge=pd.merge(df1, df2, on="A")
print(f"Merge:\n{merge}")

# 28
# Son datos que están ordenados cronológicamente
# Están asociados a un tiempo específico
Valores=(1,2,3,4,5)
series=pd.Series(Valores)
print(series)

# 29.
df=pd.DataFrame({
    "nombre": ["Juan", "Pedro", "María"],
    "edad": [20, 25, 30]
})
print(df)
df.to_csv("./src/tp1_sesiones/static/csv/taller1.csv") #dataframe a csv

# 30 

df=pd.DataFrame({
    "nombre": ["Juan", "Pedro", "Maria"],
    "edad": [20, 25, 30]
})
df.to_json("./src/tp1_sesiones/static/json/taller1dfajson.json") #dataframe a json
print(df.to_json())