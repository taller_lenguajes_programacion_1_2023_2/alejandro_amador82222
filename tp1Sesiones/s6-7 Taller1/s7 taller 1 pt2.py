import pandas as pd

#31
tabla = pd.read_html("https://www.x-rates.com/table/?from=USD&amount=1")[1]
tablaDF = pd.DataFrame(tabla)
print(tabla)

#32
tablaDF.to_excel("./src/tp1_sesiones/static/xlsx/monedas2.xlsx")

#33
primeros10 = tablaDF.head(11)
print(primeros10)

primeros10.to_csv("./src/tp1_sesiones/static/csv/monedas.csv")

#34
Hoja1=pd.read_html("https://help.netflix.com/es/node/24926")[0]
Hoja2=pd.read_html("https://www.x-rates.com/table/?from=USD&amount=1")[1]
Hoja3=pd.read_html("https://www.colombia.com/cambio-moneda/monedas-del-mundo/")[0]

with pd.ExcelWriter("./src/tp1_sesiones/static/xlsx/tresTablas.xlsx") as writer:
    Hoja1.to_excel(writer,sheet_name="Netflix")
    Hoja2.to_excel(writer,sheet_name="Monedas")
    Hoja3.to_excel(writer,sheet_name="Colombia")
print("Se han guardado las hojas")

#35
tabla = pd.DataFrame(pd.read_excel("./src/tp1_sesiones/static/xlsx/Clase4.xlsx"))
mayores30 = tabla.loc[tabla["Edad"]>=30]
print(mayores30)
mayores30.to_csv("./src/tp1_sesiones/static/csv/mayores30.csv")

#36
Archivo=pd.read_csv("./src/tp1_sesiones/static/csv/mayores30.csv")

Columnas = ['Nombre', 'Profesión']
Archivo[Columnas].to_excel("./src/tp1_sesiones/static/xlsx/NombreYProfesion.xlsx")

#37
xlsx=pd.ExcelFile("./src/tp1_sesiones/static/xlsx/tresTablas.xlsx")
Hoja1=pd.read_excel(xlsx,"Netflix")
Hoja2=pd.read_excel(xlsx,"Colombia")
Combinar=pd.concat([Hoja1,Hoja2])
Combinar.to_excel("./src/tp1_sesiones/static/xlsx/dosHojas.xlsx")
