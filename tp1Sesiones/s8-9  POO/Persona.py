from Estudios import Estudio
from Habilidad import Habilidad
from Excel import Excel

class Persona:
    def __init__(self,primernombre,segundonombre,apellido,nacionalidad,telefono,email,titulo,inicio,fin,horas,tipo,nivel,id):
        self.primernombre=primernombre
        self.segundonombre=segundonombre
        self.apellido=apellido
        self.nacionalidad=nacionalidad
        self.telefono=telefono
        self.email=email  
        self.estudio=Estudio(titulo,inicio,fin,horas)    
        self.habilidad = Habilidad(tipo,nivel,id)
        self.xlsx = Excel("hoja_vida.xlsx")
        
    def __str__(self):
        return f"Primer nombre: {self.primernombre}\n"\
            f"Segundo nombre: {self.segundonombre}\n"\
                f"Apellido: {self.apellido}\n"\
                    f"Nacionalidad: {self.nacionalidad}\n"\
                        f"Telefono: {self.telefono}\n"\
                            f"Email: {self.email}"
                            
    def setHabilidad(self, tipo, nivel, id):
        self.habilidad=Habilidad(tipo,nivel,id)
    def setEstudio(self, titulo, inicio, fin, horas):
        self.estudio=Estudio(titulo,inicio,fin,horas)
    def getHabilidad(self):
        print(self.habilidad)
    def getEstudio(self):
        print(self.estudio)